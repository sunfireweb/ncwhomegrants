# ncwhomegrants

Custom mobile-first single-page web form utilizing Bootstrap and jQuery/AJAX to collect user input, and PHP to sanitize input and create/send varied mail messages to multiple recipients.